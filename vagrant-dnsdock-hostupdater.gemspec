require_relative 'lib/version'

Gem::Specification.new do |s|
  s.name            = 'vagrant-dnsdock-hostupdater'
  s.version         = Vagrant::DnsdockHostUpdater::VERSION
  s.summary         = 'Update hosts'
  s.description     = <<-DESCRIPTION
vagrant-dnsdock-hostupdater is a Vagrant plugin which provides the ability to keep the host machines hosts file
configured to point at bridged docker containers based on container names: e.g. <container_name>.local.signal.sh.
DNSDock should be configured on the guest machine to enable containers to resolve these hosts.
  DESCRIPTION
  s.authors         = ['Brian Coit']
  s.email           = 'brian.coit@cellosignal.com'
  s.files           = Dir['{lib}/**/*'] + ['Rakefile']
  s.executables     = s.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  s.require_paths   = ['lib']
  s.homepage        = 'https://bitbucket.org/blonde/shore'
  s.license         = 'ISC'


  s.required_ruby_version = '>= 2.0.0'

  s.add_runtime_dependency 'docker-api', '~> 1.33'
# gem 'hosts', '~> 0.1.1'
  s.add_runtime_dependency 'log4r', '~> 1.1.10'
  s.add_runtime_dependency 'linebreak', '~> 2.1.0'
end