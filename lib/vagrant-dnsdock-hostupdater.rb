require 'bundler'

begin
  require 'vagrant'
rescue LoadError
  Bundler.require(:default, :development)
end

require 'vagrant-dnsdock-hostupdater/plugin'
require 'vagrant-dnsdock-hostupdater/command'