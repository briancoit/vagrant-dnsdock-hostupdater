require_relative 'host-manager'
require 'yaml'
require 'pp'

module HostManager
  class DockerEvent

    def self.update(ip, port, host_suffix)
      client = HostManager::Client.create(ip, port)
      client.host_suffix host_suffix
      client.sync_hosts Docker::Container.all({:all => true})
      client.close
    end

    def self.run(ip, port, host_suffix)
      sleep(2)
      loop {
        run_once(ip, port, host_suffix)
      }
    end

    private
    def self.run_once(ip, port, host_suffix)
      begin
        Docker::Event.stream { |event|
          HostManager.log.info 'Connected to docker socket. Listening for events.'
          # respond to all container events

          begin
            if event.type == 'container' && %w(start die).include?(event.action)
              self.update(ip, port, host_suffix)
            end

          rescue => e
            puts e.inspect
          end

          break
        }

      rescue Docker::Error::TimeoutError
        HostManager.log.info 'Docker socket connection timed out whilst listening for events. Reconnecting...'
      end
    end
  end
end

HostManager::DockerEvent.run('192.168.200.1', 2991, '.local.signal.sh')
