require 'socket' # Get sockets from stdlib
require 'json'
require_relative 'hosts/hosts'
require 'docker'
require 'logger'
require 'log4r'

module OS
  def OS.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end

  def OS.mac?
    (/darwin/ =~ RUBY_PLATFORM) != nil
  end

  def OS.unix?
    !OS.windows?
  end

  def OS.linux?
    OS.unix? and not OS.mac?
  end
end

module HostManager

  def self.log
    unless @log
      @log = Log4r::Logger.new('main')
      @log.outputters << Log4r::Outputter.stdout
      @log.outputters << Log4r::FileOutputter.new('logmain', :filename => 'host-manager.log')
    end

    @log
  end

  def self.comment_prefix
    'Managed by Vagrant plugin - do not remove.'
  end

  def self.host_file_path
    OS.windows? ? `echo %SystemRoot%\\System32\\drivers\\etc\\hosts`.chomp : '/etc/hosts'
  end

  class Base

    def log
      HostManager.log
    end

    def host_name(container_name)
      if container_name.include?('.')
        container_name
      else
        sanitize_host_value(container_name) + host_suffix
      end
    end

    def compose_host_names(container)
      names = []
      labels = container.info['Config']['Labels']

      name = labels['com.docker.compose.project']
      service = labels['com.docker.compose.service']

      if name != nil && service != nil
        service = sanitize_host_value(service)
        name = sanitize_host_value(name)

        container_num = labels['com.docker.compose.container-number']
        if container_num == '1'
          names.push("#{service}.#{name}#{host_suffix}")
        else
          names.push("#{service}-#{container_num}.#{name}#{host_suffix}")
        end
      else
        names.push(host_name(container.info['Name']))
      end

      names
    end

    def host_suffix(suffix = nil)
      if suffix != nil
        @domain_suffix = suffix
      else
        @domain_suffix
      end
    end

    # Determine if entry is managed by this utility or not
    def valid_entry?(element)
      # check it's a managed host first
      if element.respond_to?(:comment) && element.comment.to_s.start_with?(HostManager.comment_prefix)
        # check we have hostname and ip
        if element.respond_to?(:name) && element.respond_to?(:address)
          return true
        end
      end
    end

    def get_hosts
      unless @host_manager
        @host_manager = Hosts::File.read(HostManager.host_file_path)
      end

      @host_manager
    end

    def build_hosts_data(container_data)
      data = []

      if container_data && container_data.is_a?(Array)
        container_data.each do |container_info|
          container = Docker::Container.get(container_info.info['id'])

          if container.info['State']['Running']
            name = container.info['Name']

            if name
              ip = container.info['NetworkSettings']['IPAddress']

              if ip
                compose_host_names(container).each do |hostname|
                  item = {
                      :hostname => hostname,
                      :ip => ip,
                      :containerId => container.info['id']
                  }

                  data.push(item)
                end
              else
                log.warn("Ignored request to create hosts entry due to no IP being returned from container: #{name}")
              end
            end
          end

        end
      end

      data
    end

    def update_hosts_file(data)
      if data.is_a?(Hash)
        required_keys = %w(action hostname ip containerId)
        required_keys.each do |key|
          unless data.key?(key)
            raise "Data for key #{key} not provided."
          end
        end
        execute(data['action'], data['hostname'], data['ip'], data['containerId'])

      elsif data.is_a?(Array)
        hosts = get_hosts

        # initially remove all entries
        hosts.elements.delete_if do |element|
          if valid_entry?(element)
            log.info 'Removing entry for ' + element.name
            true
          end
        end

        # add recieved entries
        data.each do |entry|
          add_entry(entry, hosts)
          hosts.write
        end

        hosts.write

      end
    end

    def add_entry(entry, hosts)
      if entry[:ip] && entry[:containerId] && entry[:hostname]
        comment = HostManager.comment_prefix + (entry['containerId'] ? " Container ID: #{entry[:containerId]}" : '')
        log.info "Adding entry: #{entry[:hostname]} => #{entry[:ip]}"
        hosts.elements << Hosts::Entry.new(entry[:ip], entry[:hostname], :comment => comment)

      else
        log.warn 'Unable to write entry due to missing values [ip,containerId,hostname]: ' + entry.to_json
      end
    end

    def execute(action, hostname, ip, container_id = nil)
      hosts = get_hosts

      if action == 'create'
        hosts.elements.each do |element|
          if valid_entry?(element) && hostname == element.name
            raise "Entry already exists! (#{ip} #{hostname})"
          end
        end

        comment = HostManager.comment_prefix + (container_id ? " Container ID: #{container_id}" : '')

        hosts.elements << Hosts::Entry.new(ip, hostname, :comment => comment)
        hosts.write

      elsif action == 'delete'
        hosts.elements.delete_if do |element|
          if valid_entry?(element)
            if hostname == element.name
              log.info 'Removing entry for ' + element.name
              true
            end
          end
        end

        hosts.write
      else
        raise 'No valid action specified.'
      end
    end

    private

    def sanitize_host_value(value)
      value.sub(/^\//, '').gsub(/_/, '-')
    end

  end

  class Server < Base
    def run(ip, port)
      HostManager.log.info "Running on host manager service on #{ip}:#{port}"
      @tcp_server = TCPServer.new ip, port

      loop {

        client = @tcp_server.accept
        content = client.gets

        log.debug("Recieved data: #{content.to_s.strip}")

        begin
          unless content.to_s.chomp.strip.empty?
            data = JSON.parse(content)
            update_hosts_file(data)
          end

        rescue => e
          log.error("Caught exception attempting to execute with data #{content}. #{e}")
        end

        client.close
      }

    end

    def self.create(ip, port)
      server = self.new
      server.run(ip, port)
    end
  end

  class Client < Base

    def initialize(ip, port)
      @socket = TCPSocket.new(ip, port)
    end

    def close
      log.info('Closing client connection.')
      @socket.close
    end

    def sync_hosts(container_data)
      data = build_hosts_data(container_data)

      begin
        update_hosts_file(data)

        `service dnsmasq restart`

      rescue => e
        log.error("Caught exception attempting to write to hosts file with data #{data.to_json}. #{e}")
      end

      send(data)
    end

    def send(data)
      log.info("Sending data via client: #{data.to_json}")
      @socket.write data.to_json + "\n"
    end

    def self.create(ip, port)
      self.new(ip, port)
    end
  end

end
