module Vagrant
  module DnsdockHostUpdater

    module OS
      def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
      end

      def OS.mac?
        (/darwin/ =~ RUBY_PLATFORM) != nil
      end

      def OS.unix?
        !OS.windows?
      end

      def OS.linux?
        OS.unix? and not OS.mac?
      end
    end

    class Plugin < Vagrant.plugin('2')
      name "dnsdock-host-updater"

      description <<-DESC
          Updates hosts for docker containers on guest VM.
      DESC

      @started = false

      # Add command for `$ vagrant dnsdock-host-updater`
      # command 'dnsdock-host-updater' do
      #   require_relative 'command'
      #   Command
      # end

      def self.pid_path
        File.expand_path('~/.docker-host-manager.pid')
      end

      def self.log(msg)
        puts "[vagrant-dnsdock-hostupdater] #{msg}"
      end

      def self.pid
        File.exist?(pid_path) ? File.read(pid_path) : nil
      end

      def self.close_manager
        if pid
          log "Already running with PID: #{pid}"
          begin
            log "Attempting to stop server with PID: #{pid}"
            Process.kill('KILL', pid.to_i)
          rescue
            log "Unable to kill process with ID: #{pid}. This may be because the process has already been terminated."
          end

          log "Cleaning up PID file: #{pid_path}"
          File.delete(pid_path)
        end
      end

      def self.init_plugin
        server_path = File.expand_path('../../server.rb', __FILE__)

        if @started
          return
        end

        close_manager

        pid = Process.spawn("ruby #{server_path}")
        log "Started server with PID: #{pid}"

        if pid
          pid_file = File.open(pid_path, 'w')
          pid_file << pid
          pid_file.close
          log "Wrote server PID (#{pid}) to #{pid_path}"
          Process.detach(pid)
          @started = true
        end
      end

      action_hook(:up, :machine_action_up) { init_plugin }

    end
  end
end